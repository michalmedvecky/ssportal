from django import forms
#from captcha.fields import CaptchaField

from captcha.fields import ReCaptchaField
from .models import User
from django.conf import settings
import re
import os
from django.core.validators import RegexValidator

numeric = RegexValidator(r'^[0-9]+$', 'SMS verification code must contain only numbers')

class UserForm(forms.ModelForm):
    #captcha = CaptchaField(label="", error_messages=dict(invalid='Invalid CAPTCHA', required='CAPTCHA field is required'))
    captcha = ReCaptchaField(
        label="",
        error_messages=dict(required='Prove you\'re not a robot..'),
        required=True,
        attrs={
            'size': 'compact',
        })


    class Meta:
        model = User
        fields = (
            "fname",
            "lname",
            "email",
            "phone"
        )
        labels = {
            "fname": "First name",
            "lname": "Family name",
            "email": "E-mail",
            "phone": "Mobile"
        }
        # help_texts = {
        #     "fname": 'Make sure the field contains only letters (A-z) and dash (-).',
        #     "lname": 'Make sure the field contains only letters (A-z) and dash (-).',
        #     "email": 'Make sure you are not using a free webmail domain - that is not allowed.',
        #     "phone": 'Use the international formatting - starting with +'
        # }

    # check email blacklist
    def clean_email(self):
        email = self.cleaned_data.get('email')

        domain = email.split('@')[1]

        blacklist_file = os.path.join(settings.BASE_DIR, "portal/config/blacklist.txt")
        f = open(blacklist_file, 'r')

        for line in f:
            line = line.strip()
            if line.startswith('#'):
                continue
            elif re.search(domain, line, re.IGNORECASE):
                raise forms.ValidationError('Emails from domain %(invalid_domain)s are not allowed',
                                            code='invalid',
                                            params={
                                                'invalid_domain': domain
                                            }
                                            )
        return email

    def sanitize(self, name):

        name = name.strip()
        name = re.sub('[ ]{2,}', ' ', name)
        name = re.sub('\'', '', name)
        name = re.sub('[ ]*-[ ]*', '-', name)
        return name

    def clean_fname(self):
        fname = self.cleaned_data.get('fname')
        return self.sanitize(fname)

    def clean_lname(self):
        lname = self.cleaned_data.get('lname')
        return self.sanitize(lname)


# one time password (used only for form, no model)
class OtpForm(forms.Form):
    otp = forms.CharField(max_length=6, required=False, label="", validators=[numeric])
    captcha = ReCaptchaField(
        label="",
        error_messages=dict(required='Prove you\'re not a robot..'),
        required=True,
        attrs={
            'size' : 'compact',
    })
