-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema portalss
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema portalss
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `portalss` ;
USE `portalss` ;

-- -----------------------------------------------------
-- Table `portalss`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `portalss`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fname` VARCHAR(100) NOT NULL,
  `lname` VARCHAR(100) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `phone` VARCHAR(128) NOT NULL,
  `token` VARCHAR(255) NULL,
  `expires` DATETIME NULL,
  `verified` TINYINT(1) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
