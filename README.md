# PanIP Security: Self-service portal documentation

## Installation and configuration

#### Prerequisites 

* External dependencies (configuration is automatically created by ansible script in midportal/settings.py):
    * MySQL DB
    * SMTP server
    * SMS Gateway
    * LDAP server


#### Deployment

1. Setup prerequisites on their servers
2. Setup correct ansible variables (credentials, hosts, etc. for the prerequisites from step 1)
3. Run ansible script https://git-panip.telekom.sk/ansible-roles/ansible-ssportal

## Implementation details

* The project is implemented in Python using Django framework. https://www.djangoproject.com/
* Necessary Python libraries are specified in requirements.txt, they are automatically processed by ansible script

#### Structure

```
|-- midportal           - global project files
    |-- settings.py     - contains all global variables and configuration, automatically created when deploying by ansible script
    |-- urls.py         - the main url routing file
    |-- wsgi.py         - default django file
|-- portal              - files for app "portal"
    |-- config
        |-- blacklist.txt           - list of forbidden e-mail domains
        |-- blacklist_backup.txt    - backup blackllist for the case when downloading blacklist.txt fails (not up-to-date file)
    |-- migrations          - Django migrations (internal files)
        |-- ...
    |-- static              - graphics: css file and logo
        |-- ...
    |-- templates           - html files
        |-- ...
    |-- admin.py            - browser administration of the application (default Django file)
    |-- apps.py             - list of apps in the project (in our case only one)
    |-- forms.py            - definition of forms validation
    |-- models.py           - definition of the class User (used for storing in DB)
    |-- tests.py            - file for tests, not used (default Django file)
    |-- urls                - url routing withing the app "portal"
    |-- views.py            - controller, processing code
|-- sql
    |-- create_user.sql     - sql command creating a MySQL DB with table "user" into which class User (defined in portal/models.py) can be stored
|-- db.sqlite3              - default Django database, not used (useful for debugging in case of external DB problems)
|-- logfile.log             - log file (max size 50 kB)
|-- manage.py               - default Django file
|-- README.md
|-- requirements.txt        - pip libraries, definition for ansible deployment
```


 