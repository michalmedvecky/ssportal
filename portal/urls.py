from django.conf.urls import url
from . import views

app_name = 'portal'

urlpatterns = [
    # /portal/
    url(r'^$', views.index, name='index'),

    # /portal/create
    url(r'^create/$', views.create, name='create'),

    # /portal/sendotp
    #url(r'^bulk/$', views.bulk, name='bulk'),

    # # /portal/sendotp
    # url(r'^sendotp/$', views.sendotp, name='sendotp'),

    # /portal/verifyotp
    url(r'^verifyotp/$', views.verify_otp, name='verifyotp'),

    # /portal/activate/34/44eee003-b342-4904-af4f-ef1b6d41e6ee
    url(r'^activate/(?P<uid>[0-9]+)/(?P<token>[^/]+)/$', views.activate, name='activate'),
]
