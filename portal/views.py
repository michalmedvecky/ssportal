import smtplib
import datetime
import pytz
import pyotp
from django.shortcuts import render
from ldap3 import Tls
from .forms import UserForm, OtpForm
from .models import User
from uuid import uuid4
from ldap3 import Server, Connection, ALL
from django.conf import settings
import ssl
import logging
import urllib3
from urllib.parse import quote

log = logging.getLogger(__name__)

# Create your views here.

#
def index(request):
    log.debug("Portal started")
    return render(request, 'portal/create.html', {
        "form": UserForm(
            initial={
                "fname": "",
                "lname": "",
                "email": "",
                "phone": ""
            }
        )
    })


# called from create()
def get_user(form):
    user = User()
    user.fname = form.cleaned_data['fname']
    user.lname = form.cleaned_data['lname']
    user.email = form.cleaned_data['email']
    user.phone = form.cleaned_data['phone']
    user.token = uuid4()

    log.debug(
        "Create: fname = " + user.fname + ", lname = " + user.lname + ", email = " + user.email + ", phone = " + str(
            user.phone))
    log.info("User create  (turn debug ON for more details)")
    return user


# /create/
def create(request):
    if request.method == 'POST':
        form = UserForm(request.POST)

        if 'reload' in request.POST:
            return render(request, 'portal/create.html', {'form': form, 'hide': True})

        if form.is_valid():
            user = get_user(form)
            user.save()
            request.session['uid'] = user.id

            log.info("User id:" + str(user.id) + ", entries are valid")
            log.info("Calling sendotp")

            return send_otp(request)
    else:
        form = UserForm()

    # not valid, retry
    log.debug("Invalid entry")
    return render(request, 'portal/create.html', {"form": form })


def send_otp(request):
    # /sendotp
    if 'uid' not in request.session:
        log.debug("uid not in request")
        return render(request, 'portal/create.html', { "form": UserForm() })

    uid = request.session['uid']
    user = User.objects.get(pk=uid)
    log.debug("got uid: " + str(uid))

    # generate secret and timestamp
    secret = pyotp.random_base32()
    created_at = datetime.datetime.now()

    # send secret to veryfyotp()
    request.session['secret'] = secret
    request.session['created_at'] = created_at.isoformat()

    # generate otp
    totp = pyotp.TOTP(secret)
    otp = totp.at(created_at)

    sms_text = 'Registration OTP: ' + str(otp)
    mobile_number = str(user.phone).replace('+','00')

    sms_status = send_sms(sms_text, mobile_number)

    if sms_status != "0" and sms_status != "3":
        # FIXME: add a error message so the user know what happened
        # use .messages(request, "Internal error, the SMS was unable to be sent")
        return render(request, 'portal/create.html', {"form": UserForm()})

    return render(request, 'portal/verifyotp.html', {'otpform': OtpForm(), 'user': user})


def verify_otp(request):
    # /verifyotp
    # get data from POST and session
    otpform = OtpForm(request.POST)

    if 'uid' not in request.session:
        log.debug("verifyotp: uid not in session")
        form = UserForm()
        return render(request, 'portal/create.html', {"form": form})

    uid = request.session['uid']
    user = User.objects.get(pk=uid)

    if not otpform.is_valid():
        return render(request, 'portal/verifyotp.html',
                      {'otpform': otpform, 'user': user})

    if 'resend' in request.POST:
        log.debug("verifyotp: Button resend pushed")
        return send_otp(request)

    else:
        secret = request.session['secret']
        created_at = datetime.datetime.strptime(request.session['created_at'], "%Y-%m-%dT%H:%M:%S.%f")

        totp = pyotp.TOTP(secret)
        if otpform.is_valid():
            otp = otpform.cleaned_data['otp']

            if totp.verify(otp, created_at):
                #if True:
                log.debug("verifyotp: User id:" + str(user.id) + ", otp match!")

                send_email_status = send_email(User.objects.get(pk=request.session['uid']))
                if send_email_status:
                    return render(request, 'portal/sendemail.html')
                else:
                    return render(request, 'portal/sendemail_error.html')
            else:
                # no match, retry
                log.debug("verifyotp: User id:" + str(user.id) + ", Invalid otp: " + otp)
                otpform.add_error("otp", "SMS OTP is invalid")

                log.debug('verifyotp: Unable to verify')

    return render(request, 'portal/verifyotp.html',
                  {'otpform': otpform, 'user': user})


# called from verifyotp()
def send_email(user):
    # set expires
    today = datetime.datetime.now()
    user.expires = today + datetime.timedelta(hours=24)
    user.save()

    url = settings.DOMAIN + "/activate/" + str(user.id) + "/" + user.token
    full_url = settings.HTTP_SCHEME + str(url).replace('//', '/')
    email_text = "From: %s\nTo: %s\nSubject: %s\n\n%s\n\n%s\n\n%s" % (settings.EMAIL_SENDER, user.email, settings.EMAIL_SUBJECT, settings.EMAIL_BODY, str(full_url), settings.EMAIL_SIGNATURE)

    try:
        log.debug(email_text)

        server = smtplib.SMTP(host=settings.EMAIL_HOST, port=settings.EMAIL_PORT, timeout=5)
        server.ehlo()
        server.sendmail(settings.EMAIL_HOST_USER, user.email, email_text)
        server.close()

        log.info("User id:" + str(user.id) + ", email sent")
        return True

    except Exception as err:
        log.error("User id:" + str(user.id) + ", unable to send email to " + user.email)
        log.debug("Exception while trying to send email: " + str(err))
        return False



def send_to_ldap(user):
# called from activate()
    try:
        # TLS
        if settings.LDAP_SSL:
            tls = Tls(ca_certs_file=settings.CA_CERTS_FILE,
                      validate=ssl.CERT_REQUIRED,
                      version=ssl.PROTOCOL_TLSv1)
            server = Server(host=settings.LDAP_HOST, port=settings.LDAP_PORT, use_ssl=True, tls=tls)
        else:
            server = Server(host=settings.LDAP_HOST, port=settings.LDAP_PORT, use_ssl=False)
        conn = Connection(server, user=settings.LDAP_USER, password=settings.LDAP_PSWD, auto_bind=True)
        conn.bind()

        # change DN !!!
        ## FIXME: THIS SHOULD BE RANDOM!
        #dn_text = "cn=" + user.fname + " " + user.lname + ", " + settings.LDAP_USERS_DN
        dn_text = "cn=" + str(user.id) + ", " + settings.LDAP_USERS_DN

        ldap_add_log = conn.add(dn_text, ['inetOrgPerson'], attributes={'givenName': user.fname, 'sn': user.lname,
                                                       'mobile': str(user.phone), 'mail': user.email})

        if ldap_add_log:
            log.debug("User id:" + str(user.id) + ", user added to DN: " + dn_text)
            return True
        else:
            log.debug("ERROR adding user id:" + str(user.id) + " to ldap. Fishy.")
            log.debug(ldap_add_log.result)

        return False

    except Exception as e:
        log.error("User id:" + str(user.id) + ", unable to connect to LDAP host: " + settings.LDAP_HOST)
        log.error(e)
        return False


# /activate/34/44eee00...
def activate(request, uid, token):
    try:
        utc = pytz.UTC
        user = User.objects.get(id=uid, token=token)
        now = utc.localize(datetime.datetime.now())
        if user.expires > now and user.verified == False:
            log.debug("User id:" + str(user.id) + " sending to ldap")
            if send_to_ldap(user):
                user.verified = True
                user.save()
                log.info("User id: " + str(user.id) + " activated")
                return render(request, 'portal/activated.html', {'user': user})
            else:
                log.error("Unable to connect to LDAP host!")
                return render(request, 'portal/activated_error.html')

            # to delete:
            # return render(request, 'portal/activated.html', {'user': user})

    except Exception as e:
        log.error(e)
        return render(request, 'portal/expired.html')

    log.debug("User id:" + str(user.id) + ", activation link expired or already active.")
    return render(request, 'portal/expired.html')


def send_sms(smsContent, phonenumber):
    try:
        http = urllib3.ProxyManager(settings.PROXY)
    except:
        log.info("Proxy setting not found while loading sending SMS, will try without it")
        http = urllib3.PoolManager()

    appUrl = settings.SMS_APP_URL
    username = settings.SMS_USER
    password = settings.SMS_PSWD

    requesturl = appUrl + '?username=' + username + '&password=' + password + '&to=' + phonenumber \
                 + '&charset=UTF-8&coding=2&text=' + quote(smsContent)
    log.debug("URL to be requested: " + str(requesturl))

    try:
        r = http.request('GET', requesturl)

        status_line = r.data.decode('utf-8')
        status_code = status_line.split(":")[0].strip()
        log.info("SendSMS: XXXX" + str(phonenumber)[-6:] + ", SMS Gateway returned: " + str(status_line))
        log.debug("SendSMS: " + str(phonenumber) + ", SMS Gateway returned: " + str(status_line))
        log.debug("HTTP request: \n" +  str(r.data.decode('utf-8')))
        return status_code

    except Exception as e:
        log.error("Error: " + str(e))


# def bulk(request):
#     # return a textarea form
#     #
#     # the form must be
#     # use built-in python csv modul
#     # create array of Users
#     # loop over lines, parse the lines, create user object