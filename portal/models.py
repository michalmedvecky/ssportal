from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from django.core.validators import RegexValidator

fname = RegexValidator(r'^[a-zA-Z \-\']+$', 'First name can contain letters (A-z) and dash (-) only')
lname = RegexValidator(r'^[a-zA-Z \-\']+$', 'Family name can contain letters (A-z) and dash (-) only')

class User(models.Model):
    fname = models.CharField(max_length=100, validators=[fname])
    lname = models.CharField(max_length=100, validators=[lname])
    email = models.EmailField(max_length=100)
    phone = PhoneNumberField()
    token = models.CharField(max_length=255, null=True)
    expires = models.DateTimeField(null=True)
    verified = models.BooleanField(default=False)

    class Meta:
        managed = True
        db_table = 'user'


