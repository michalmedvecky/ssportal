import patterns as patterns
from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'', include('portal.urls'))
]
